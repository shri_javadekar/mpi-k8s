#!/usr/bin/env python3

import time
import sys
import subprocess
import chevron
import os
import paramiko
from shutil import copyfile
import logging

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.DEBUG,datefmt='%d-%b-%y %H:%M:%S')

def runCommand(cmd):
    logging.debug("Executing command: " + cmd)
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    return output, error

def getCliArg(index, defaultVal):
    if len(sys.argv) > index:
        return sys.argv[index]
    else:
        return defaultVal

def main():
    # TODO:
    # 1. Test it on non default namespaces
    # 2. Create samples for small, medium, large executions
    # 3. Push to gitlab
    namespace = getCliArg(1, "default")
    logging.info("Using namespace: " + namespace)

    mpiCommand = getCliArg(2, "mpirun -env STRESS_TIMEOUT=30s -n 4 -hostfile /etc/mpi-hosts/mpi-hosts mpi_hello_world")
    logging.info("Using mpiCommand: " + mpiCommand)

    deleteOnCompletion = getCliArg(3, "")
    logging.info("Using deleteOnCompletion: " + deleteOnCompletion)

    #0. Actually create the files from the templates
    with open('statefulset.yaml.tmpl', 'r') as f:
        fileContent = chevron.render(f, {'mpiCommand': mpiCommand})
        with open('statefulset.yaml', 'w') as of:
            of.write(fileContent)

    #1. Create a new ssh keypair in the local directory. TODO: Maybe keep the keys around somewhere if needed?
    os.remove("./id_rsa") if os.path.exists("./id_rsa") else None
    os.remove("./id_rsa.pub") if os.path.exists("./id_rsa.pub") else None
    os.remove("./authorized_keys") if os.path.exists("./authorized_keys") else None
    key = paramiko.RSAKey.generate(4096)
    key.write_private_key_file("./id_rsa")

    # write the public key
    pub = paramiko.RSAKey(filename="./id_rsa")
    with open("./id_rsa.pub", 'w') as f:
        f.write("%s %s" % (pub.get_name(), pub.get_base64()))

    #cmd = "ssh-keygen -t rsa -f ./id_rsa"
    #runCommand(cmd)
    copyfile("./id_rsa.pub", "./authorized_keys")
    logging.info("Created new ssh keys for the job in local directory")

    #1. Delete the old secret and create a new one with the new keys
    cmd = "kubectl delete secret ssh-keys --ignore-not-found -n " + namespace
    output, err = runCommand(cmd)
    if err != None:
        print("Failed to delete secret: " + err)
        return

    cmd = "kubectl create secret generic ssh-keys --from-file=./id_rsa --from-file=./id_rsa.pub --from-file=./authorized_keys --from-file=config -n {}".format(namespace)
    runCommand(cmd)
    logging.info("Mounted ssh keys into Kubernetes as a secret")

    #2. Setup the RBAC
    with open('rbac.yaml.tmpl', 'r') as f:
        fileContent = chevron.render(f, {'namespace': namespace})
        with open('rbac.yaml', 'w') as of:
            of.write(fileContent)    
    cmd = "kubectl auth reconcile -f rbac.yaml"
    runCommand(cmd)
    logging.info("Created Kubernetes RBAC for the new mpi task")

    #3. Create the statefulset
    cmd = "kubectl delete -f statefulset.yaml -n " + namespace
    runCommand(cmd)
    cmd = "kubectl wait pod mpi-sts-0 --for=delete --timeout=300s -n " + namespace
    runCommand(cmd)

    cmd = "kubectl apply -f statefulset.yaml -n " + namespace
    runCommand(cmd)
    logging.info("Created the statefulset. The mpi task should start shortly...")

    #3. Wait for the job to be completed.
    jobCompleted = False
    while jobCompleted != True:
        cmd = "kubectl get pods -n {} --selector=mpi-task-status=done".format(namespace)
        output, error = runCommand(cmd)
        if "mpi-sts-0".encode() in output:
           jobCompleted = True 
        else:
           logging.info("waiting...\n")
           time.sleep(10)

    #4. Delete the statefulset and the secret
    if deleteOnCompletion != "":
        cmd = "kubectl delete -f statefulset.yaml -n " + namespace
        runCommand(cmd)
        cmd = "kubectl delete secret ssh-keys --ignore-not-found -n " + namespace
        runCommand(cmd)        
 
if __name__ == "__main__":
    main()

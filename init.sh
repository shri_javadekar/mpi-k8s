#!/bin/bash

set -x
/usr/sbin/sshd -D &

cp /tmp/ssh-keys/* /root/.ssh/
while ! [ -s /tmp/mpi-task-dir/ips ]; do
    sleep 10
    echo "Waiting for getting ips ..."
    kubectl get pods -l task=$MPI_TASK_LABEL -o json  | jq -r .items[].status.podIP > /tmp/mpi-task-dir/ips

    # if the total number of ips does not match the required number, keep waiting
    a=`wc -l < /tmp/mpi-task-dir/ips`
    if [[ $a -ne $MPI_JOB_COUNT ]]; then
        echo "Not all pods have an IP address yet $a/$MPI_JOB_COUNT"
        rm /tmp/mpi-task-dir/ips
        continue
    else
        echo "All IPs found $a/$MPI_JOB_COUNT"
    fi

    # if any of the lines in the file is not an IP, delete the /tmp/mpi-task-dir/ips and try again
    while read ip; do
      if [[ $ip =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
         echo $ip "is a valid IP"
      else
         echo $ip" is not valid IP"
         rm /tmp/mpi-task-dir/ips
         break
      fi
    done </tmp/mpi-task-dir/ips
    echo "All IPs are valid"
done

mkdir /tmp/mpi-task-dir/.ssh
while read p; do
  ssh-keyscan $p >> /tmp/mpi-task-dir/.ssh/known_hosts
done </tmp/mpi-task-dir/ips

chmod 600 /tmp/mpi-task-dir/.ssh/*
chmod 600 /tmp/mpi-task-dir/.ssh


FROM docker.io/shrinand/mpi-base

RUN apt-get install stress-ng -y && apt-get install curl -y

COPY code/mpi_hello_world_ubuntu /usr/local/bin/mpi_hello_world
RUN chmod 777 /usr/local/bin/mpi_hello_world

RUN curl -LO https://dl.k8s.io/release/v1.22.0/bin/linux/amd64/kubectl && \
    chmod +x kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

COPY entrypoint.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh

# mpi-k8s



## This repo contains a demo of how MPI jobs could be run on Kubernetes


- Uses statefulsets for actually running the mpi jobs
- The pods communicate with each other using ssh keys generated specifically for one job
- Uses configMap for the host names of the statefulset pods

## Pre-requisities

- MacOSX or other Unix system. This code has been tested on MacOSX.
- Kubernetes cluster with admin privileges
- Kubeconfig of the cluster should be downloaded and the context should be setup (kubectl commands should work correctly)
- kubectl command line tool working correctly against the cluster
- Python3.x and the following packages (use pip3 install <package-name> for installing them)
  - chevron
  - paramiko
  - shutil


## Steps to run

- Clone this git repo onto your local machine

```
$ cd mpi-k8s
$ ./run.py default "mpirun -env STRESS_TIMEOUT=60s -n 4 -hostfile /etc/mpi-hosts/mpi-hosts mpi_hello_world" deleteOnCompletion
```

- The order of the arguments is important:
    - The first argument is the name of the namespace in the K8s cluster.
    - The second argument is the mpirun command that should be run. Since the docker image being run has the mpi_hello_world binary available in it, the above example shows how to use it. If another application needs to be run, then a new docker image needs to be built based on the current one.
    - The _deleteOnCompletion_ flag is used to indicate whether the statefulset and the ssh keys should be deleted after the task is completed or not. Feel free to run *without* flag to keep the statefulset around even after the mpirun command has completed it's execution.

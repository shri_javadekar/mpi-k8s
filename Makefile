EXECS=mpi_hello_world
MPICC?=mpicc

all: ${EXECS}

mpi_hello_world: mpi_hello_world.c
	${MPICC} -o mpi_hello_world mpi_hello_world.c

base:
	docker build -f Dockerfile.base . -t shrinand/mpi-base

app:
	docker build -f Dockerfile . -t shrinand/mpi-app

test-apps:
	docker build -f Dockerfile.mpi4py . -t shrinand/mpi-test-apps

init:
	docker build -f Dockerfile.init . -t shrinand/mpi-app-init

#!/bin/bash

set -x
/usr/sbin/sshd -D &

cp /tmp/ssh-keys/* /root/.ssh/
cp /tmp/mpi-task-dir/.ssh/known_hosts /root/.ssh/
cp /tmp/mpi-task-dir/ips /etc/mpi-hosts

chmod 600 /root/.ssh/*
chmod 600 /root/.ssh

if [ "$HOSTNAME" = "mpi-sts-0" ] || [ "$MPI_RUN" = "true" ] ; then
    printf '%s\n' "on the right host"

    kubectl label pods $HOSTNAME "mpi-task-status=running"

    # Just wait for sometime here so that all pods are up
    sleep 60
    echo "mpi-hosts file"
    cat /etc/mpi-hosts
    chmod 777 /etc/mpi-hosts

    if [[ -z "${MPI_COMMAND}" ]]; then
      timeout 2h mpirun -env STRESS_TIMEOUT=30s -n 4 -hostfile /etc/mpi-hosts mpi_hello_world
    else
      timeout 2h ${MPI_COMMAND}
    fi

    if [ $? -eq 0 ]; then
        result="success"
    else
        result="failed"
        echo "mpitask failed..."
        exit 1
    fi

    kubectl label pods $HOSTNAME --overwrite "mpi-task-result=$result"
    # Add a label on the pod indicating that it's command has completed
    kubectl label pods $HOSTNAME --overwrite "mpi-task-status=done"
else
    printf '%s\n' "uh-oh, not on 0th"
    wait
fi


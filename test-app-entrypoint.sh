#!/bin/bash

set -x
/usr/sbin/sshd -D &

cp /tmp/ssh-keys/* /root/.ssh/
cp /tmp/mpi-task-dir/.ssh/known_hosts /root/.ssh/
cp /tmp/mpi-task-dir/ips /etc/mpi-hosts

chmod 600 /root/.ssh/*
chmod 600 /root/.ssh

if [ "$MPI_RUN" = "true" ] ; then
    printf '%s\n' "on the right host"

    # Just wait for sometime here so that all pods are up
    sleep 60
    echo "mpi-hosts file"
    cat /etc/mpi-hosts
    chmod 777 /etc/mpi-hosts

    if [[ -z "${MPI_COMMAND}" ]]; then
      echo "No MPI_COMMAND specified. Exiting ..."
      exit 1
    else
      ${MPI_COMMAND}
    fi

    if [ $? -eq 0 ]; then
        echo "mpitask succeeded"
    else
        result="failed"
        echo "mpitask failed..."
        exit 1
    fi
else
    printf '%s\n' "uh-oh, not on 0th"
    wait
fi


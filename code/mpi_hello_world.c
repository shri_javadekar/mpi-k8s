// Author: Wes Kendall
// Copyright 2011 www.mpitutorial.com
// This code is provided freely with the tutorials on mpitutorial.com. Feel
// free to modify it for your own use. Any distribution of the code must
// either provide a link to www.mpitutorial.com or keep this header intact.
//
// An intro MPI hello world program that uses MPI_Init, MPI_Comm_size,
// MPI_Comm_rank, MPI_Finalize, and MPI_Get_processor_name.
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int run_stress_ng() {
  char *default_ts = "10s";
  char *stress_timeout = getenv("STRESS_TIMEOUT");
  char cmd_prefix[100] = "stress-ng -c 0 --timeout ";
  char *cmd;

  if(stress_timeout) {
      printf("Var found: %s\n", stress_timeout);
  } else {
      stress_timeout = default_ts;
      printf("Var set to default: %s\n", stress_timeout);
  }

  cmd = strcat(cmd_prefix, stress_timeout);
  system(cmd);

  return 0;
}

int main(int argc, char** argv) {
  // Initialize the MPI environment. The two arguments to MPI Init are not
  // currently used by MPI implementations, but are there in case future
  // implementations might need the arguments.
  MPI_Init(NULL, NULL);

  // Get the number of processes
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  // Print off a hello world message
  printf("Hello world from processor %s, rank %d out of %d processors\n",
         processor_name, world_rank, world_size);

  run_stress_ng();
 
  // Finalize the MPI environment. No more MPI calls can be made after this
  MPI_Finalize();
}
